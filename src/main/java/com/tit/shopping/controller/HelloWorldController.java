package com.tit.shopping.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/hello")
public class HelloWorldController {

    @GetMapping("/shopping")
    @ResponseBody
    public String helloWorld(){
        return "欢迎光临,Tit商城!后续功能待开发！";
    }
}
