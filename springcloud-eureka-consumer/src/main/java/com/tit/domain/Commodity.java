package com.tit.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Commodity {
    private Integer id;

    private String name;

    private Double price;

    private String inventory;

    private String describe;

    private Date times;
}
