package com.tit.remote;


import com.tit.domain.Commodity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@FeignClient(name="eureka-service")
public interface CommodityRemote {

    @GetMapping("/Hello/finAll")
    List<Commodity> findAll();

    @GetMapping("/Hello/{id}")
    Commodity gitComById (@PathVariable("id") int id);

    @GetMapping("/Hello/delete/{id}")
    void delete(@PathVariable("id") int id);

    @PostMapping("/Hello/save")
    void save(Commodity commodity);

}
