package com.tit.controller;

import com.tit.domain.Commodity;
import com.tit.remote.CommodityRemote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/Hello")
public class ConsumerController {
    @Autowired
    private LoadBalancerClient loadBalancerClient;

    @Autowired
    private RestTemplate restTemplate;

    @Resource
    CommodityRemote commodityRemote;

    @GetMapping
    public ModelAndView list(Model model) {
        model.addAttribute("priceList", commodityRemote.findAll());
        model.addAttribute("title","商品管理");
        return new ModelAndView("commodity/list","commodityModel",model);
    }

    @RequestMapping("/Consumer")
    public String helloWorld(String s){
        System.out.println("传入的值为："+s);
        //第一种调用方式
        //String forObject = new RestTemplate().getForObject("http://localhost:8071/Hello/World?s=" + s, String.class);

        //第二种调用方式
        //根据服务名 获取服务列表 根据算法选取某个服务 并访问某个服务的网络位置。
        //ServiceInstance serviceInstance = loadBalancerClient.choose("EUREKA-SERVICE");
        //String forObject = new RestTemplate().getForObject("http://"+serviceInstance.getHost()+":"+serviceInstance.getPort()+"/Hello/World?s="+s,String.class);

        //第三种调用方式 需要restTemplate注入的方式
        String forObject = restTemplate.getForObject("http://EUREKA-SERVICE/Hello/World?s=" + s, String.class);
        return forObject;
    }
    @RequestMapping("/finAll")
    public List<Commodity> finAll() {
        List<Commodity> commodityList = commodityRemote.findAll();
        //String forObject = restTemplate.getForObject("http://EUREKA-SERVICE/Hello/finAll", String.class);
        return commodityList;
    }

    @GetMapping("{id}")
    public ModelAndView view(@PathVariable("id") int id,Model model){
        Commodity commodity = commodityRemote.gitComById(id);
        //User user = new User(1,"ddd","ddd");
        model.addAttribute("commodity", commodity);
        model.addAttribute("title","查看用户");
        return new ModelAndView("commodity/view","commoditymodel",model);
    }

    @GetMapping("/delete/{id}")
    public void delete(@PathVariable("id") int id){
        commodityRemote.delete(id);
    }

    @GetMapping("/modify/{id}")
    public ModelAndView modify(@PathVariable("id") int id,Model model){
        Commodity commodity =commodityRemote.gitComById(id);
        model.addAttribute("commodity", commodity);
        model.addAttribute("title","修改商品");
        return new ModelAndView("commodity/form","commodityModel",model);
    }

    @PostMapping("/save")
    public ModelAndView save(Commodity commodity) {
        commodityRemote.save(commodity);
        return new ModelAndView("redirect:/Hello");
    }

}
