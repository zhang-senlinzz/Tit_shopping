package com.tit.service;

import com.tit.Dao.CommodityDao;
import com.tit.domain.Commodity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CommodityService {

    @Autowired
    CommodityDao commodityDao;

    public void save(Commodity commodity){
        if(commodity.getId() != null){
            commodityDao.deleteById(commodity.getId().toString());
        }
        commodity.setTimes(new Date());
        commodityDao.save(commodity);
    }

    public List<Commodity> findAll(){
        return commodityDao.findAll();
    }

    public Commodity gitComById(Integer id){
        Optional<Commodity> commodity_op = commodityDao.findById(Integer.toString(id));
        return commodity_op.get();
    }

    public void delete(Integer id){
        commodityDao.deleteById(Integer.toString(id));
    }

    public void update(Commodity commodity){
        String id = commodity.getId().toString();
        commodityDao.deleteById(id);
        commodity.setTimes(new Date());
        commodityDao.save(commodity);;
    }
}
