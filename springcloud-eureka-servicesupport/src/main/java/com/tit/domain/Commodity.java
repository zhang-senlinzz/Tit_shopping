package com.tit.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "commodity")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Commodity implements Serializable {

    @Id
    private Integer id;

    private String name;

    private Double price;

    private String inventory;

    private String describe;

    private Date times;

}
