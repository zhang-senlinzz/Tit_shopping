package com.tit.controller;

import com.tit.domain.Commodity;
import com.tit.service.CommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Hello")
public class ControllerTest {

    @Autowired
    CommodityService commodityService;

    @RequestMapping("/World")
    public String helloWorld(String s){
        System.out.println("传入的值为："+s);
        return "传入的值为："+s;
    }

    @RequestMapping("/finAll")
    public List<Commodity> finAll(){
        return commodityService.findAll();
    }

    @GetMapping("{id}")
    public Commodity gitComById (@PathVariable("id") int id){
        return commodityService.gitComById(id);
    }

    @GetMapping("/delete/{id}")
    public void delete(@PathVariable("id") int id){
        commodityService.delete(id);
    }

    @PostMapping("/save")
    public void save(Commodity commodity) {
        commodityService.save(commodity);
    }

}
