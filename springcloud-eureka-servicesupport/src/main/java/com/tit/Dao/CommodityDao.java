package com.tit.Dao;

import com.tit.domain.Commodity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CommodityDao extends JpaRepository<Commodity, String>, JpaSpecificationExecutor<Commodity> {
}
